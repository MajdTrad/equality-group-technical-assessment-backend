﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SW_API.Models
{
    //SWAPI response model
    public class SWAPIResponseModel
    {
        public string count { get; set; }
        public string next { get; set; }
        public string previous { get; set; }
        public List<Starship> results { get; set; }
    }
}