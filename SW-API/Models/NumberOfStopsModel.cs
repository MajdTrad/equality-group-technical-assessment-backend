﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SW_API.Models
{
    //This model will be sent to the client app
    public class NumberOfStopsModel
    {
        public string name { get; set; }
        public string stops { get; set; }
    }
}