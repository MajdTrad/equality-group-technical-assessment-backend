﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using SW_API.Models;

namespace SW_API.Helpers
{
    public class SWAPIHelper
    {
        static HttpClient client = new HttpClient();
        
        public static async Task<List<Starship>> GetAllStarships()
        {
            //Initialize http client
            client.BaseAddress = new Uri("https://swapi.dev/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                // Get all starships
                List<Starship> starships = await GetStarshipsAsync("starships/");
                client = new HttpClient();
                return starships;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        static async Task<List<Starship>> GetStarshipsAsync(string path)
        {
            List<Starship> starships = null;
            SWAPIResponseModel responseModel = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                responseModel = await response.Content.ReadAsAsync<SWAPIResponseModel>();
                starships = responseModel.results.ToList();
            }
            return starships;
        }
    }
}