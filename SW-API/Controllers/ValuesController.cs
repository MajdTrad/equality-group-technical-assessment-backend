﻿using SW_API.Helpers;
using SW_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SW_API.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values?distance=x Get number of stops for each starship based on distance
        public async System.Threading.Tasks.Task<HttpResponseMessage> GetStarships(string distance = "1000000")
        {

            List<Starship> starships = await SWAPIHelper.GetAllStarships();
            List<NumberOfStopsModel> returnValue = new List<NumberOfStopsModel>();
            float MGLT = float.Parse(distance);
            foreach(Starship s in starships)
            {
                NumberOfStopsModel temp = new NumberOfStopsModel();
                temp.name = s.name;

                float amount = 0;
                amount = float.Parse(s.consumables.Split()[0]);
                
                //Convert consumables to hours
                if(s.consumables.ToLower().Contains("year"))
                {
                    //8760 hours in a year
                    amount = amount * 8760;
                }
                else if(s.consumables.ToLower().Contains("month"))
                {
                    //Approximately 730 hours in a month (average)
                    amount = amount * 730;
                }
                else if(s.consumables.ToLower().Contains("week"))
                {
                    //168 hours in a week
                    amount = amount * 168;
                }
                else if(s.consumables.ToLower().Contains("day"))
                {
                    //24 hours in a day
                    amount = amount * 24;
                }

                //Number of hours required for the starship to reach destination
                float hours = (MGLT) / (float.Parse(s.MGLT));

                //Number of stops required to reach destination
                float stops = (hours) / (amount);
                //Rounds the number of stops down - 1.5 stops is technically 1 stop
                temp.stops = (System.Math.Floor(stops)).ToString();

                returnValue.Add(temp);
            }
            return Request.CreateResponse(HttpStatusCode.OK, returnValue);
        }
    }
}
